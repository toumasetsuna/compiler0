	.arch armv8-a
	.arch_extension crc
	.arm
	.global main
	.type main , %function
main:
push {fp}
mov fp, sp
sub sp, sp, #8
.L8:
	ldr v12, =10
	str v12, [fp, #-4]
	ldr v13, =0
	sub v4, v13, #1
	str v4, [fp, #-8]
	ldr v5, [fp, #-4]
	ldr v6, [fp, #-8]
	add v11, v5, v6
	mov r0, v11
	add sp, sp, #8
	push {fp}
	bx lr
