	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word 3
	.global b
	.align 4
	.size b, 4
b:
	.word 5
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L7:
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	add r4, r5, r6
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
addr_a:
	.word a
addr_b:
	.word b
