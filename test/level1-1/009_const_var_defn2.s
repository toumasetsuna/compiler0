	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global b
	.align 4
	.size b, 4
b:
	.word 5
	.global a
	.align 4
	.size a, 4
a:
	.word 10
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L5:
	ldr r4, addr_b
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
addr_b:
	.word b
addr_a:
	.word a
