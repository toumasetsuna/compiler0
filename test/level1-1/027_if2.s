	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word 0
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L4:
	ldr r4, =10
	ldr r5, addr_a
	str r4, [r5]
	ldr r4, addr_a
	ldr r5, [r4]
	cmp r5, #0
	bgt  .L5
	b .L11
.L5:
	mov r0, #1
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
	b .L7
.L6:
	mov r0, #0
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
	b .L7
.L7:
	mov r0, #0
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
.L9:
	mov r0, #0
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
.L10:
	mov r0, #0
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
.L11:
	b .L6
addr_a:
	.word a
