	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word 0
	.global b
	.align 4
	.size b, 4
b:
	.word 0
	.global c
	.align 4
	.size c, 4
c:
	.word 0
	.global d
	.align 4
	.size d, 4
d:
	.word 0
	.global e
	.align 4
	.size e, 4
e:
	.word 0
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L55:
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_a
	str r4, [r5]
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_b
	str r4, [r5]
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_c
	str r4, [r5]
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_d
	str r4, [r5]
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_e
	str r4, [r5]
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	ldr r4, addr_c
	ldr r7, [r4]
	mul r4, r6, r7
	sub r6, r5, r4
	ldr r4, addr_d
	ldr r5, [r4]
	ldr r4, addr_a
	ldr r7, [r4]
	ldr r4, addr_c
	ldr r8, [r4]
	sdiv r4, r7, r8
	sub r7, r5, r4
	cmp r6, r7
	bne  .L57
	b .L70
.L57:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L58
.L58:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L60:
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	add r4, r5, r6
	ldr r5, addr_c
	ldr r6, [r5]
	add r5, r4, r6
	ldr r4, addr_d
	ldr r6, [r4]
	ldr r4, addr_e
	ldr r7, [r4]
	add r4, r6, r7
	cmp r5, r4
	beq  .L57
	b .L84
.L62:
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	mul r4, r5, r6
	ldr r5, addr_c
	ldr r6, [r5]
	sdiv r5, r4, r6
	ldr r4, addr_e
	ldr r6, [r4]
	ldr r4, addr_d
	ldr r7, [r4]
	add r4, r6, r7
	cmp r5, r4
	beq  .L57
	b .L77
.L68:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L69:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L70:
	b .L62
.L75:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L76:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L77:
	b .L60
.L82:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L83:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L84:
	b .L58
addr_a:
	.word a
addr_b:
	.word b
addr_c:
	.word c
addr_d:
	.word d
addr_e:
	.word e
