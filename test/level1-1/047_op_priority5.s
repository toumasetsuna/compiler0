	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word 1
	.global b
	.align 4
	.size b, 4
b:
	.word 0
	.global c
	.align 4
	.size c, 4
c:
	.word 1
	.global d
	.align 4
	.size d, 4
d:
	.word 2
	.global e
	.align 4
	.size e, 4
e:
	.word 4
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L50:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	mul r4, r5, r6
	ldr r5, addr_c
	ldr r6, [r5]
	sdiv r5, r4, r6
	ldr r4, addr_e
	ldr r6, [r4]
	ldr r4, addr_d
	ldr r7, [r4]
	add r4, r6, r7
	cmp r5, r4
	beq  .L57
	b .L64
.L52:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L53
.L53:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L55:
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	ldr r4, addr_c
	ldr r7, [r4]
	mul r4, r6, r7
	sub r6, r5, r4
	ldr r4, addr_d
	ldr r5, [r4]
	ldr r4, addr_a
	ldr r7, [r4]
	ldr r4, addr_c
	ldr r8, [r4]
	sdiv r4, r7, r8
	sub r7, r5, r4
	cmp r6, r7
	beq  .L52
	b .L80
.L57:
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_a
	ldr r6, [r4]
	ldr r4, addr_b
	ldr r7, [r4]
	add r4, r6, r7
	mul r6, r5, r4
	ldr r4, addr_c
	ldr r5, [r4]
	add r4, r6, r5
	ldr r5, addr_d
	ldr r6, [r5]
	ldr r5, addr_e
	ldr r7, [r5]
	add r5, r6, r7
	cmp r4, r5
	ble  .L52
	b .L72
.L62:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L63:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L64:
	b .L55
.L70:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L71:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L72:
	b .L55
.L78:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L79:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L80:
	b .L53
addr_a:
	.word a
addr_b:
	.word b
addr_c:
	.word c
addr_d:
	.word d
addr_e:
	.word e
