	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global k
	.align 4
	.size k, 4
k:
	.word 0
	.global n
	.align 4
	.size n, 4
n:
	.word 10
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L23:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, =1
	ldr r5, addr_k
	str r4, [r5]
	b .L25
.L25:
	ldr r4, [fp, #-4]
	ldr r5, addr_n
	ldr r6, [r5]
	sub r5, r6, #1
	cmp r4, r5
	ble  .L26
	b .L32
.L26:
	ldr r4, [fp, #-4]
	add r5, r4, #1
	str r5, [fp, #-4]
	ldr r4, addr_k
	ldr r5, [r4]
	add r4, r5, #1
	ldr r4, addr_k
	ldr r5, [r4]
	ldr r4, addr_k
	ldr r6, [r4]
	add r4, r5, r6
	ldr r5, addr_k
	str r4, [r5]
	b .L25
.L27:
	ldr r4, addr_k
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, addr_k
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L30:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L31:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L32:
	b .L27
addr_k:
	.word k
addr_n:
	.word n
