	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L28:
	ldr r4, =56
	str r4, [fp, #-4]
	ldr r4, =4
	str r4, [fp, #-8]
	ldr r4, [fp, #-4]
	ldr r5, =0
	sub r6, r5, #4
	sub r5, r4, r6
	ldr r4, [fp, #-8]
	add r6, r5, r4
	str r6, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #0
	cmp v36, #0
	cmp v37, #0
	cmp v38, #0
	bne  .L39
	b .L41
.L33:
	ldr r4, =0
	sub r5, r4, #1
	ldr r4, =0
	sub r6, r4, r5
	ldr r4, =0
	sub r5, r4, r6
	str r5, [fp, #-4]
	b .L35
.L34:
	ldr r4, [fp, #-8]
	ldr r5, =0
	add r6, r5, r4
	str r6, [fp, #-4]
	b .L35
.L35:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L39:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L40:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L41:
	b .L40
