	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global a
	.align 4
	.size a, 4
a:
	.word 0
	.global b
	.align 4
	.size b, 4
b:
	.word 0
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L18:
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_a
	str r4, [r5]
	bl getint
	add sp, sp, #0
	mov r4, r0
	ldr r5, addr_b
	str r4, [r5]
	ldr r4, addr_a
	ldr r5, [r4]
	ldr r4, addr_b
	ldr r6, [r4]
	cmp r5, r6
	beq  .L24
	b .L28
.L20:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L22
.L21:
	ldr r4, =0
	str r4, [fp, #-4]
	b .L22
.L22:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L24:
	ldr r4, addr_a
	ldr r5, [r4]
	cmp r5, #3
	bne  .L20
	b .L32
.L26:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L27:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L28:
	b .L21
.L30:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L31:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L32:
	b .L21
addr_a:
	.word a
addr_b:
	.word b
