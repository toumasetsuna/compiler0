	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #20
.L46:
	ldr r4, =5
	str r4, [fp, #-20]
	ldr r4, =5
	str r4, [fp, #-16]
	ldr r4, =1
	str r4, [fp, #-12]
	ldr r4, =0
	sub r5, r4, #2
	str r5, [fp, #-8]
	ldr r4, =2
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	ldr r5, =1
	mul r6, r4, r5
	ldr r4, =2
	sdiv r5, r6, r4
	cmp r5, #0
	blt  .L52
	b .L61
.L52:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	b .L53
.L53:
	ldr r4, [fp, #-8]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	add r4, r5, #67
	cmp r4, #0
	blt  .L75
	b .L84
.L55:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-16]
	sub r6, r4, r5
	cmp r6, #0
	bne  .L63
	b .L68
.L59:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L60:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L61:
	b .L55
.L63:
	ldr r4, [fp, #-12]
	add r5, r4, #3
	ldr r4, =2
	sdiv r6, r5, r4
	mul r7, r6, r4
	sub r4, r5, r7
	sub r4, r5, r7
	cmp r4, #0
	bne  .L52
	b .L74
.L66:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L67:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L68:
	b .L53
.L72:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L73:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L74:
	b .L53
.L75:
	ldr r4, =4
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	b .L76
.L76:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L78:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-16]
	sub r6, r4, r5
	cmp r6, #0
	bne  .L86
	b .L91
.L82:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L83:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L84:
	b .L78
.L86:
	ldr r4, [fp, #-12]
	add r5, r4, #2
	ldr r4, =2
	sdiv r6, r5, r4
	mul r7, r6, r4
	sub r4, r5, r7
	sub r4, r5, r7
	cmp r4, #0
	bne  .L75
	b .L97
.L89:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L90:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L91:
	b .L76
.L95:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L96:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L97:
	b .L76
