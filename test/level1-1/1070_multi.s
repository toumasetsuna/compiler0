	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L16:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, =0
	str r4, [fp, #-8]
	b .L19
.L19:
	ldr r4, [fp, #-8]
	cmp r4, #21
	blt  .L20
	b .L25
.L20:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	mul r6, r4, r5
	str r6, [fp, #-4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L19
.L21:
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L23:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L24:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L25:
	b .L21
