	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #16
.L46:
	ldr r4, =0
	str r4, [fp, #-16]
	ldr r4, =0
	str r4, [fp, #-12]
	ldr r4, =0
	str r4, [fp, #-8]
	b .L51
.L51:
	ldr r4, [fp, #-16]
	cmp r4, #21
	blt  .L52
	b .L57
.L52:
	b .L58
.L53:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L55:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L56:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L57:
	b .L53
.L58:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	ldr r6, =101
	sub r7, r6, r5
	cmp r4, r7
	blt  .L59
	b .L65
.L59:
	ldr r4, [fp, #-16]
	ldr r5, =100
	sub r6, r5, r4
	ldr r4, [fp, #-12]
	sub r5, r6, r4
	str r5, [fp, #-8]
	ldr r4, [fp, #-16]
	ldr r5, =5
	mul r6, r5, r4
	ldr r4, [fp, #-12]
	ldr r5, =1
	mul r7, r5, r4
	add r4, r6, r7
	ldr r5, [fp, #-8]
	ldr r6, =2
	sdiv r7, r5, r6
	add r5, r4, r7
	cmp r5, #100
	beq  .L68
	b .L78
.L60:
	ldr r4, [fp, #-16]
	add r5, r4, #1
	str r5, [fp, #-16]
	b .L51
.L63:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L64:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L65:
	b .L60
.L68:
	ldr r4, [fp, #-16]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, [fp, #-12]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, [fp, #-8]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L69
.L69:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L58
.L76:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L77:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L78:
	b .L69
