	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global defn
	.type defn , %function
defn:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L5:
	mov r0, #4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L6:
	bl defn
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
