	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global ifElse
	.type ifElse , %function
ifElse:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L11:
	ldr r4, =5
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	cmp r4, #5
	beq  .L13
	b .L19
.L13:
	ldr r4, =25
	str r4, [fp, #-4]
	b .L15
.L14:
	ldr r4, [fp, #-4]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-4]
	b .L15
.L15:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L17:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L18:
	mov r0, #0
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
.L19:
	b .L14
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L21:
	bl ifElse
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
