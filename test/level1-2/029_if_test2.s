	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global ifElseIf
	.type ifElseIf , %function
ifElseIf:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L34:
	ldr r4, =5
	str r4, [fp, #-8]
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	cmp r4, #6
	beq  .L37
	b .L45
.L37:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
	b .L39
.L38:
	ldr r4, [fp, #-4]
	cmp r4, #10
	beq  .L54
	b .L58
.L39:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L41:
	ldr r4, [fp, #-4]
	cmp r4, #11
	beq  .L37
	b .L49
.L43:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L44:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L45:
	b .L41
.L47:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L48:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L49:
	b .L38
.L50:
	ldr r4, =25
	str r4, [fp, #-8]
	b .L52
.L51:
	ldr r4, [fp, #-4]
	cmp r4, #10
	beq  .L67
	b .L71
.L52:
	b .L39
.L54:
	ldr r4, [fp, #-8]
	cmp r4, #1
	beq  .L50
	b .L62
.L56:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L57:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L58:
	b .L51
.L60:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L61:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L62:
	b .L51
.L63:
	ldr r4, [fp, #-8]
	add r5, r4, #15
	str r5, [fp, #-8]
	b .L65
.L64:
	ldr r4, [fp, #-8]
	ldr r5, =0
	sub r6, r5, r4
	str r6, [fp, #-8]
	b .L65
.L65:
	b .L52
.L67:
	ldr r4, [fp, #-8]
	ldr r5, =0
	sub r6, r5, #5
	cmp r4, r6
	beq  .L63
	b .L75
.L69:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L70:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L71:
	b .L64
.L73:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L74:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L75:
	b .L64
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L77:
	bl ifElseIf
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
