	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global if_if_Else
	.type if_if_Else , %function
if_if_Else:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L15:
	ldr r4, =5
	str r4, [fp, #-8]
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	cmp r4, #5
	beq  .L18
	b .L24
.L18:
	ldr r4, [fp, #-4]
	cmp r4, #10
	beq  .L25
	b .L30
.L19:
	ldr r4, [fp, #-8]
	add r5, r4, #15
	str r5, [fp, #-8]
	b .L20
.L20:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L22:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L23:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L24:
	b .L19
.L25:
	ldr r4, =25
	str r4, [fp, #-8]
	b .L26
.L26:
	b .L20
.L28:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L29:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L30:
	b .L26
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L32:
	bl if_if_Else
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
