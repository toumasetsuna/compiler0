	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global FourWhile
	.type FourWhile , %function
FourWhile:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #16
.L46:
	ldr r4, =5
	str r4, [fp, #-16]
	ldr r4, =6
	str r4, [fp, #-12]
	ldr r4, =7
	str r4, [fp, #-8]
	ldr r4, =10
	str r4, [fp, #-4]
	b .L51
.L51:
	ldr r4, [fp, #-16]
	cmp r4, #20
	blt  .L52
	b .L57
.L52:
	ldr r4, [fp, #-16]
	add r5, r4, #3
	str r5, [fp, #-16]
	b .L59
.L53:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-12]
	ldr r6, [fp, #-4]
	add r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-8]
	add r6, r5, r4
	mov r0, r6
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L55:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L56:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L57:
	b .L53
.L59:
	ldr r4, [fp, #-12]
	cmp r4, #10
	blt  .L60
	b .L65
.L60:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L67
.L61:
	ldr r4, [fp, #-12]
	sub r5, r4, #2
	str r5, [fp, #-12]
	b .L51
.L63:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L64:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L65:
	b .L61
.L67:
	ldr r4, [fp, #-8]
	cmp r4, #7
	beq  .L68
	b .L73
.L68:
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	str r5, [fp, #-8]
	b .L75
.L69:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L59
.L71:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L72:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L73:
	b .L69
.L75:
	ldr r4, [fp, #-4]
	cmp r4, #20
	blt  .L76
	b .L81
.L76:
	ldr r4, [fp, #-4]
	add r5, r4, #3
	str r5, [fp, #-4]
	b .L75
.L77:
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-4]
	b .L67
.L79:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L80:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L81:
	b .L77
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L89:
	bl FourWhile
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
