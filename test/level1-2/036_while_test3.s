	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global g
	.align 4
	.size g, 4
g:
	.word 0
	.global h
	.align 4
	.size h, 4
h:
	.word 0
	.global f
	.align 4
	.size f, 4
f:
	.word 0
	.global e
	.align 4
	.size e, 4
e:
	.word 0
	.text
	.global EightWhile
	.type EightWhile , %function
EightWhile:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #16
.L94:
	ldr r4, =5
	str r4, [fp, #-16]
	ldr r4, =6
	str r4, [fp, #-12]
	ldr r4, =7
	str r4, [fp, #-8]
	ldr r4, =10
	str r4, [fp, #-4]
	b .L99
.L99:
	ldr r4, [fp, #-16]
	cmp r4, #20
	blt  .L100
	b .L105
.L100:
	ldr r4, [fp, #-16]
	add r5, r4, #3
	str r5, [fp, #-16]
	b .L107
.L101:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-12]
	ldr r6, [fp, #-4]
	add r7, r5, r6
	add r5, r4, r7
	ldr r4, [fp, #-8]
	add r6, r5, r4
	ldr r4, addr_e
	ldr r5, [r4]
	ldr r4, [fp, #-4]
	add r7, r5, r4
	ldr r4, addr_g
	ldr r5, [r4]
	sub r4, r7, r5
	ldr r5, addr_h
	ldr r7, [r5]
	add r5, r4, r7
	sub r4, r6, r5
	mov r0, r4
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L103:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L104:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L105:
	b .L101
.L107:
	ldr r4, [fp, #-12]
	cmp r4, #10
	blt  .L108
	b .L113
.L108:
	ldr r4, [fp, #-12]
	add r5, r4, #1
	str r5, [fp, #-12]
	b .L115
.L109:
	ldr r4, [fp, #-12]
	sub r5, r4, #2
	str r5, [fp, #-12]
	b .L99
.L111:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L112:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L113:
	b .L109
.L115:
	ldr r4, [fp, #-8]
	cmp r4, #7
	beq  .L116
	b .L121
.L116:
	ldr r4, [fp, #-8]
	sub r5, r4, #1
	str r5, [fp, #-8]
	b .L123
.L117:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L107
.L119:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L120:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L121:
	b .L117
.L123:
	ldr r4, [fp, #-4]
	cmp r4, #20
	blt  .L124
	b .L129
.L124:
	ldr r4, [fp, #-4]
	add r5, r4, #3
	str r5, [fp, #-4]
	b .L131
.L125:
	ldr r4, [fp, #-4]
	sub r5, r4, #1
	str r5, [fp, #-4]
	b .L115
.L127:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L128:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L129:
	b .L125
.L131:
	ldr r4, addr_e
	ldr r5, [r4]
	cmp r5, #1
	bgt  .L132
	b .L137
.L132:
	ldr r4, addr_e
	ldr r5, [r4]
	sub r4, r5, #1
	ldr r5, addr_e
	str r4, [r5]
	b .L139
.L133:
	ldr r4, addr_e
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_e
	str r4, [r5]
	b .L123
.L135:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L136:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L137:
	b .L133
.L139:
	ldr r4, addr_f
	ldr r5, [r4]
	cmp r5, #2
	bgt  .L140
	b .L145
.L140:
	ldr r4, addr_f
	ldr r5, [r4]
	sub r4, r5, #2
	ldr r5, addr_f
	str r4, [r5]
	b .L147
.L141:
	ldr r4, addr_f
	ldr r5, [r4]
	add r4, r5, #1
	ldr r5, addr_f
	str r4, [r5]
	b .L131
.L143:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L144:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L145:
	b .L141
.L147:
	ldr r4, addr_g
	ldr r5, [r4]
	cmp r5, #3
	blt  .L148
	b .L153
.L148:
	ldr r4, addr_g
	ldr r5, [r4]
	add r4, r5, #10
	ldr r5, addr_g
	str r4, [r5]
	b .L155
.L149:
	ldr r4, addr_g
	ldr r5, [r4]
	sub r4, r5, #8
	ldr r5, addr_g
	str r4, [r5]
	b .L139
.L151:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L152:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L153:
	b .L149
.L155:
	ldr r4, addr_h
	ldr r5, [r4]
	cmp r5, #10
	blt  .L156
	b .L161
.L156:
	ldr r4, addr_h
	ldr r5, [r4]
	add r4, r5, #8
	ldr r5, addr_h
	str r4, [r5]
	b .L155
.L157:
	ldr r4, addr_h
	ldr r5, [r4]
	sub r4, r5, #1
	ldr r5, addr_h
	str r4, [r5]
	b .L147
.L159:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L160:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L161:
	b .L157
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L177:
	ldr r4, =1
	ldr r5, addr_g
	str r4, [r5]
	ldr r4, =2
	ldr r5, addr_h
	str r4, [r5]
	ldr r4, =4
	ldr r5, addr_e
	str r4, [r5]
	ldr r4, =6
	ldr r5, addr_f
	str r4, [r5]
	bl EightWhile
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
addr_g:
	.word g
addr_h:
	.word h
addr_f:
	.word f
addr_e:
	.word e
