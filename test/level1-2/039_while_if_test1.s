	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global whileIf
	.type whileIf , %function
whileIf:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L21:
	ldr r4, =0
	str r4, [fp, #-8]
	ldr r4, =0
	str r4, [fp, #-4]
	b .L24
.L24:
	ldr r4, [fp, #-8]
	cmp r4, #100
	blt  .L25
	b .L30
.L25:
	ldr r4, [fp, #-8]
	cmp r4, #5
	beq  .L31
	b .L37
.L26:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L28:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L29:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L30:
	b .L26
.L31:
	ldr r4, =25
	str r4, [fp, #-4]
	b .L33
.L32:
	ldr r4, [fp, #-8]
	cmp r4, #10
	beq  .L38
	b .L44
.L33:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L24
.L35:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L36:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L37:
	b .L32
.L38:
	ldr r4, =42
	str r4, [fp, #-4]
	b .L40
.L39:
	ldr r4, [fp, #-8]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-4]
	b .L40
.L40:
	b .L33
.L42:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L43:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L44:
	b .L39
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L47:
	bl whileIf
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
