	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global ifWhile
	.type ifWhile , %function
ifWhile:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L25:
	ldr r4, =0
	str r4, [fp, #-8]
	ldr r4, =3
	str r4, [fp, #-4]
	ldr r4, [fp, #-8]
	cmp r4, #5
	beq  .L28
	b .L34
.L28:
	b .L35
.L29:
	b .L44
.L30:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L32:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L33:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L34:
	b .L29
.L35:
	ldr r4, [fp, #-4]
	cmp r4, #2
	beq  .L36
	b .L41
.L36:
	ldr r4, [fp, #-4]
	add r5, r4, #2
	str r5, [fp, #-4]
	b .L35
.L37:
	ldr r4, [fp, #-4]
	add r5, r4, #25
	str r5, [fp, #-4]
	b .L30
.L39:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L40:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L41:
	b .L37
.L44:
	ldr r4, [fp, #-8]
	cmp r4, #5
	blt  .L45
	b .L50
.L45:
	ldr r4, [fp, #-4]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-4]
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L44
.L46:
	b .L30
.L48:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L49:
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
.L50:
	b .L46
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #0
.L53:
	bl ifWhile
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #0
	pop {lr}
	pop {fp}
	bx lr
