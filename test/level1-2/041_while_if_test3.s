	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global deepWhileBr
	.type deepWhileBr , %function
deepWhileBr:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #20
str r0, [fp, #-16]
str r1, [fp, #-20]
.L33:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-20]
	add r6, r4, r5
	str r6, [fp, #-12]
	b .L38
.L38:
	ldr r4, [fp, #-12]
	cmp r4, #75
	blt  .L39
	b .L44
.L39:
	ldr r4, =42
	str r4, [fp, #-8]
	ldr r4, [fp, #-12]
	cmp r4, #100
	blt  .L46
	b .L51
.L40:
	ldr r4, [fp, #-12]
	mov r0, r4
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L42:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L43:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L44:
	b .L40
.L46:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	add r6, r4, r5
	str r6, [fp, #-12]
	ldr r4, [fp, #-12]
	cmp r4, #99
	bgt  .L53
	b .L58
.L47:
	b .L38
.L49:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L50:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L51:
	b .L47
.L53:
	ldr r4, [fp, #-8]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-4]
	ldr r4, =1
	cmp r4, #1
	beq  .L61
	b .L66
.L54:
	b .L47
.L56:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L57:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L58:
	b .L54
.L61:
	ldr r4, [fp, #-4]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-12]
	b .L62
.L62:
	b .L54
.L64:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L65:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L66:
	b .L62
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
.L68:
	ldr r4, =2
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-4]
	mov r0, r5
	mov r1, r4
	bl deepWhileBr
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
