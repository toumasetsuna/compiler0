	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global fun
	.type fun , %function
fun:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #12
str r0, [fp, #-8]
str r1, [fp, #-12]
.L30:
	b .L34
.L34:
	ldr r4, [fp, #-12]
	cmp r4, #0
	bgt  .L35
	b .L40
.L35:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	str r5, [fp, #-4]
	ldr r4, [fp, #-12]
	str r4, [fp, #-8]
	ldr r4, [fp, #-4]
	str r4, [fp, #-12]
	b .L34
.L36:
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L38:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L39:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L40:
	b .L36
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #12
.L42:
	bl getint
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	bl getint
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	mov r0, r5
	mov r1, r4
	bl fun
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
