	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global s
	.align 4
	.size s, 4
s:
	.word 0
	.text
	.global get_ans_se
	.type get_ans_se , %function
get_ans_se:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #16
str r0, [fp, #-8]
str r1, [fp, #-12]
str r2, [fp, #-16]
.L252:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	cmp r4, r5
	beq  .L257
	b .L262
.L257:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L258
.L258:
	ldr r4, [fp, #-8]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-8]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	add r6, r4, r5
	str r6, [fp, #-8]
	ldr r4, addr_s
	ldr r5, [r4]
	ldr r4, [fp, #-8]
	add r6, r5, r4
	ldr r4, addr_s
	str r6, [r4]
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L260:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L261:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L262:
	b .L258
	.text
	.global get_ans
	.type get_ans , %function
get_ans:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #16
str r0, [fp, #-8]
str r1, [fp, #-12]
str r2, [fp, #-16]
.L266:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-16]
	cmp r4, r5
	beq  .L271
	b .L276
.L271:
	ldr r4, =1
	str r4, [fp, #-4]
	b .L272
.L272:
	ldr r4, [fp, #-8]
	ldr r5, =2
	mul r6, r4, r5
	str r6, [fp, #-8]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	add r6, r4, r5
	str r6, [fp, #-8]
	ldr r4, [fp, #-8]
	mov r0, r4
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L274:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L275:
	mov r0, #0
	add sp, sp, #16
	pop {lr}
	pop {fp}
	bx lr
.L276:
	b .L272
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #36
.L279:
	ldr r4, =0
	sub r5, r4, #-2147483648
	str r5, [fp, #-36]
	ldr r4, =-2147483648
	str r4, [fp, #-32]
	ldr r4, =-2147483648
	add r5, r4, #1
	str r5, [fp, #-28]
	ldr r4, =2147483647
	str r4, [fp, #-24]
	ldr r4, =2147483647
	sub r5, r4, #1
	str r5, [fp, #-20]
	ldr r4, [fp, #-32]
	ldr r5, [fp, #-36]
	mov r0, #0
	mov r1, r5
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-36]
	add r6, r5, #1
	ldr r5, [fp, #-4]
	mov r0, r5
	mov r1, r6
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-24]
	ldr r5, =0
	sub r6, r5, r4
	sub r4, r6, #1
	ldr r5, [fp, #-36]
	ldr r6, [fp, #-4]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-20]
	add r5, r4, #1
	ldr r4, [fp, #-36]
	ldr r6, [fp, #-4]
	mov r0, r6
	mov r1, r4
	mov r2, r5
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-28]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, [fp, #-32]
	ldr r5, =2
	sdiv r7, r4, r5
	ldr r4, [fp, #-4]
	mov r0, r4
	mov r1, r7
	mov r2, r6
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-24]
	ldr r5, =0
	sub r6, r5, r4
	sub r4, r6, #1
	ldr r5, [fp, #-32]
	ldr r6, [fp, #-4]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-20]
	add r5, r4, #1
	ldr r4, [fp, #-32]
	ldr r6, [fp, #-4]
	mov r0, r6
	mov r1, r4
	mov r2, r5
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-28]
	mov r0, #0
	mov r1, r5
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-28]
	ldr r6, [fp, #-8]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-24]
	ldr r6, [fp, #-8]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-32]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, [fp, #-36]
	ldr r5, =2
	sdiv r7, r4, r5
	ldr r4, [fp, #-8]
	mov r0, r4
	mov r1, r7
	mov r2, r6
	bl get_ans
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-8]
	ldr r4, [fp, #-32]
	ldr r5, [fp, #-36]
	mov r0, #0
	mov r1, r5
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-28]
	ldr r5, [fp, #-36]
	add r6, r5, #1
	ldr r5, [fp, #-12]
	mov r0, r5
	mov r1, r6
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-24]
	ldr r5, =0
	sub r6, r5, r4
	sub r4, r6, #1
	ldr r5, [fp, #-36]
	ldr r6, [fp, #-12]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-20]
	add r5, r4, #1
	ldr r4, [fp, #-36]
	ldr r6, [fp, #-12]
	mov r0, r6
	mov r1, r4
	mov r2, r5
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-28]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, [fp, #-32]
	ldr r5, =2
	sdiv r7, r4, r5
	ldr r4, [fp, #-12]
	mov r0, r4
	mov r1, r7
	mov r2, r6
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-24]
	ldr r5, =0
	sub r6, r5, r4
	sub r4, r6, #1
	ldr r5, [fp, #-32]
	ldr r6, [fp, #-12]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-20]
	add r5, r4, #1
	ldr r4, [fp, #-32]
	ldr r6, [fp, #-12]
	mov r0, r6
	mov r1, r4
	mov r2, r5
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-12]
	ldr r4, [fp, #-24]
	ldr r5, [fp, #-28]
	mov r0, #0
	mov r1, r5
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-16]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-28]
	ldr r6, [fp, #-16]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-16]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-24]
	ldr r6, [fp, #-16]
	mov r0, r6
	mov r1, r5
	mov r2, r4
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-16]
	ldr r4, [fp, #-32]
	ldr r5, =2
	sdiv r6, r4, r5
	ldr r4, [fp, #-36]
	ldr r5, =2
	sdiv r7, r4, r5
	ldr r4, [fp, #-16]
	mov r0, r4
	mov r1, r7
	mov r2, r6
	bl get_ans_se
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-16]
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	add r6, r4, r5
	ldr r4, [fp, #-12]
	add r5, r6, r4
	ldr r4, [fp, #-16]
	add r6, r5, r4
	mov r0, r6
	add sp, sp, #36
	pop {lr}
	pop {fp}
	bx lr
addr_s:
	.word s
