	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global d
	.align 4
	.size d, 4
d:
	.word 0
	.global b
	.align 4
	.size b, 4
b:
	.word 0
	.global a
	.align 4
	.size a, 4
a:
	.word 0
	.text
	.global set_a
	.type set_a , %function
set_a:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
str r0, [fp, #-4]
.L169:
	ldr r4, [fp, #-4]
	ldr r5, addr_a
	str r4, [r5]
	ldr r4, addr_a
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
	.text
	.global set_b
	.type set_b , %function
set_b:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
str r0, [fp, #-4]
.L171:
	ldr r4, [fp, #-4]
	ldr r5, addr_b
	str r4, [r5]
	ldr r4, addr_b
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
	.text
	.global set_d
	.type set_d , %function
set_d:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #4
str r0, [fp, #-4]
.L173:
	ldr r4, [fp, #-4]
	ldr r5, addr_d
	str r4, [r5]
	ldr r4, addr_d
	ldr r5, [r4]
	mov r0, r5
	add sp, sp, #4
	pop {lr}
	pop {fp}
	bx lr
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #24
.L175:
	ldr r4, =2
	ldr r5, addr_a
	str r4, [r5]
	ldr r4, =3
	ldr r5, addr_b
	str r4, [r5]
	mov r0, #0
	bl set_a
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L176:
	b .L177
.L177:
	ldr r4, addr_a
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #32
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, addr_b
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #32
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, =2
	ldr r5, addr_a
	str r4, [r5]
	ldr r4, =3
	ldr r5, addr_b
	str r4, [r5]
	mov r0, #0
	bl set_a
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L179:
	mov r0, #1
	bl set_b
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L180:
	b .L181
.L181:
	ldr r4, addr_a
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #32
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, addr_b
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #10
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, =1
	str r4, [fp, #-24]
	ldr r4, =2
	ldr r5, addr_d
	str r4, [r5]
	ldr r4, [fp, #-24]
	cmp r4, #1
	bge  .L188
	b .L192
.L183:
	mov r0, #1
	bl set_b
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L185:
	b .L186
.L186:
	ldr r4, addr_d
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #32
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, [fp, #-24]
	cmp r4, #1
	ble  .L193
	b .L200
.L188:
	mov r0, #3
	bl set_d
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L190:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L191:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L192:
	b .L186
.L193:
	b .L194
.L194:
	ldr r4, addr_d
	ldr r5, [r4]
	mov r0, r5
	bl putint
	add sp, sp, #0
	mov r4, r0
	mov r0, #10
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, =2
	add r5, r4, #1
	ldr r4, =3
	sub r6, r4, r5
	ldr r4, =16
	cmp r4, r6
	bge  .L201
	b .L208
.L196:
	mov r0, #4
	bl set_d
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L198:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L199:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L200:
	b .L196
.L201:
	mov r0, #65
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L202
.L202:
	ldr r4, =25
	sub r5, r4, #7
	ldr r4, =6
	ldr r6, =3
	mul r7, r4, r6
	ldr r4, =36
	sub r6, r4, r7
	cmp r5, r6
	bne  .L209
	b .L217
.L206:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L207:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L208:
	b .L202
.L209:
	mov r0, #66
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L210
.L210:
	ldr r4, =1
	cmp r4, #8
	blt  .L222
	b .L224
	ldr r4, =7
	ldr r5, =2
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	cmp v221, r5
	bne  .L218
	b .L228
.L215:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L216:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L217:
	b .L210
.L218:
	mov r0, #67
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L219
.L219:
	ldr r4, =3
	cmp r4, #4
	bgt  .L233
	b .L235
	cmp v232, #0
	beq  .L229
	b .L238
.L222:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L223:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L224:
	b .L223
.L226:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L227:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L228:
	b .L219
.L229:
	mov r0, #68
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L230
.L230:
	ldr r4, =1
	cmp r4, #102
	beq  .L243
	b .L245
	cmp v242, #63
	ble  .L239
	b .L248
.L233:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L234:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L235:
	b .L234
.L236:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L237:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L238:
	b .L230
.L239:
	mov r0, #69
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L240
.L240:
	ldr r4, =5
	sub r5, r4, #6
	ldr r4, =0
	cmp r4, #0
	cmp v253, #0
	bne  .L254
	b .L256
	cmp r5, v95
	beq  .L249
	b .L259
.L243:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L244:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L245:
	b .L244
.L246:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L247:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L248:
	b .L240
.L249:
	mov r0, #70
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L250
.L250:
	mov r0, #10
	bl putch
	add sp, sp, #0
	mov r4, r0
	ldr r4, =4
	str r4, [fp, #-20]
	ldr r4, =3
	str r4, [fp, #-16]
	ldr r4, =2
	str r4, [fp, #-12]
	ldr r4, =1
	str r4, [fp, #-8]
	ldr r4, =0
	str r4, [fp, #-4]
	b .L265
.L254:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L255:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L256:
	b .L255
.L257:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L258:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L259:
	b .L250
.L265:
	ldr r4, [fp, #-4]
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L266:
	mov r0, #32
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L265
.L267:
	ldr r4, [fp, #-4]
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L269:
	ldr r4, [fp, #-8]
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L270:
	mov r0, #67
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L271
.L271:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	cmp r4, r5
	bge  .L274
	b .L281
.L273:
	ldr r4, [fp, #-8]
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L274:
	mov r0, #72
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L275
.L275:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	cmp r4, r5
	bge  .L289
	b .L293
.L277:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	cmp r4, r5
	ble  .L274
	b .L285
.L279:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L280:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L281:
	b .L277
.L283:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L284:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L285:
	b .L275
.L286:
	mov r0, #73
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L287
.L287:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	cmp r5, #0
	cmp r4, v305
	beq  .L303
	b .L308
.L289:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-16]
	cmp r4, r5
	bne  .L286
	b .L297
.L291:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L292:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L293:
	b .L287
.L295:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L296:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L297:
	b .L287
.L298:
	mov r0, #74
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L299
.L299:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	cmp r5, #0
	cmp r4, v322
	beq  .L317
	b .L325
.L301:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-20]
	cmp r4, r5
	bge  .L298
	b .L316
.L303:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-16]
	cmp r4, r5
	blt  .L298
	b .L312
.L306:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L307:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L308:
	b .L301
.L310:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L311:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L312:
	b .L301
.L314:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L315:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L316:
	b .L299
.L317:
	mov r0, #75
	bl putch
	add sp, sp, #0
	mov r4, r0
	b .L318
.L318:
	mov r0, #10
	bl putch
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L320:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-16]
	cmp r4, r5
	blt  .L327
	b .L331
.L323:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L324:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L325:
	b .L320
.L327:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-20]
	cmp r4, r5
	bge  .L317
	b .L335
.L329:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L330:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L331:
	b .L318
.L333:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L334:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L335:
	b .L318
addr_d:
	.word d
addr_b:
	.word b
addr_a:
	.word a
