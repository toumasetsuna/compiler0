	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global fsqrt
	.type fsqrt , %function
fsqrt:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #12
str r0, [fp, #-12]
.L36:
	ldr r4, =0
	str r4, [fp, #-8]
	ldr r4, [fp, #-12]
	ldr r5, =2
	sdiv r6, r4, r5
	str r6, [fp, #-4]
	b .L41
.L41:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-4]
	sub r6, r4, r5
	cmp r6, #0
	bne  .L42
	b .L48
.L42:
	ldr r4, [fp, #-4]
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	ldr r6, [fp, #-8]
	sdiv r7, r5, r6
	add r5, r4, r7
	str r5, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, =2
	sdiv r6, r4, r5
	str r6, [fp, #-4]
	b .L41
.L43:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L46:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L47:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L48:
	b .L43
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L52:
	ldr r4, =400
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	mov r0, r4
	bl fsqrt
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putch
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
