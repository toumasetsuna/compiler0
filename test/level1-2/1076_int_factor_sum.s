	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global N
	.align 4
	.size N, 4
N:
	.word 0
	.global newline
	.align 4
	.size newline, 4
newline:
	.word 0
	.text
	.global factor
	.type factor , %function
factor:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #12
str r0, [fp, #-12]
.L31:
	ldr r4, =0
	str r4, [fp, #-4]
	ldr r4, =1
	str r4, [fp, #-8]
	b .L35
.L35:
	ldr r4, [fp, #-8]
	ldr r5, [fp, #-12]
	add r6, r5, #1
	cmp r4, r6
	blt  .L36
	b .L42
.L36:
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	cmp r5, #0
	beq  .L43
	b .L49
.L37:
	ldr r4, [fp, #-4]
	mov r0, r4
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L40:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L41:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L42:
	b .L37
.L43:
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	add r6, r4, r5
	str r6, [fp, #-4]
	b .L44
.L44:
	ldr r4, [fp, #-8]
	add r5, r4, #1
	str r5, [fp, #-8]
	b .L35
.L47:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L48:
	mov r0, #0
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
.L49:
	b .L44
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #12
.L52:
	ldr r4, =4
	ldr r5, addr_N
	str r4, [r5]
	ldr r4, =10
	ldr r5, addr_newline
	str r4, [r5]
	ldr r4, =1478
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	mov r0, r4
	bl factor
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #12
	pop {lr}
	pop {fp}
	bx lr
addr_N:
	.word N
addr_newline:
	.word newline
