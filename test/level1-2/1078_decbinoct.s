	.arch armv8-a
	.arch_extension crc
	.arm
	.text
	.global dec2bin
	.type dec2bin , %function
dec2bin:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #20
str r0, [fp, #-20]
.L40:
	ldr r4, =0
	str r4, [fp, #-16]
	ldr r4, =1
	str r4, [fp, #-12]
	ldr r4, [fp, #-20]
	str r4, [fp, #-4]
	b .L46
.L46:
	ldr r4, [fp, #-4]
	cmp r4, #0
	bne  .L47
	b .L52
.L47:
	ldr r4, [fp, #-4]
	ldr r5, =2
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	str r5, [fp, #-8]
	ldr r4, [fp, #-12]
	ldr r5, [fp, #-8]
	mul r6, r4, r5
	ldr r4, [fp, #-16]
	add r5, r6, r4
	str r5, [fp, #-16]
	ldr r4, [fp, #-12]
	ldr r5, =10
	mul r6, r4, r5
	str r6, [fp, #-12]
	ldr r4, [fp, #-4]
	ldr r5, =2
	sdiv r6, r4, r5
	str r6, [fp, #-4]
	b .L46
.L48:
	ldr r4, [fp, #-16]
	mov r0, r4
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L50:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L51:
	mov r0, #0
	add sp, sp, #20
	pop {lr}
	pop {fp}
	bx lr
.L52:
	b .L48
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L58:
	ldr r4, =400
	str r4, [fp, #-8]
	ldr r4, [fp, #-8]
	mov r0, r4
	bl dec2bin
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putint
	add sp, sp, #0
	mov r4, r0
	ldr r4, =10
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	mov r0, r4
	bl putch
	add sp, sp, #0
	mov r4, r0
	mov r0, #0
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
