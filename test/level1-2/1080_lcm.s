	.arch armv8-a
	.arch_extension crc
	.arm
	.data
	.global n
	.align 4
	.size n, 4
n:
	.word 0
	.text
	.global gcd
	.type gcd , %function
gcd:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #24
str r0, [fp, #-20]
str r1, [fp, #-24]
.L50:
	ldr r4, [fp, #-20]
	str r4, [fp, #-16]
	ldr r4, [fp, #-24]
	str r4, [fp, #-12]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-24]
	cmp r4, r5
	blt  .L57
	b .L62
.L57:
	ldr r4, [fp, #-20]
	str r4, [fp, #-8]
	ldr r4, [fp, #-24]
	str r4, [fp, #-20]
	ldr r4, [fp, #-8]
	str r4, [fp, #-24]
	b .L58
.L58:
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-24]
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	str r5, [fp, #-4]
	b .L64
.L60:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L61:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L62:
	b .L58
.L64:
	ldr r4, [fp, #-4]
	cmp r4, #0
	bne  .L65
	b .L70
.L65:
	ldr r4, [fp, #-24]
	str r4, [fp, #-20]
	ldr r4, [fp, #-4]
	str r4, [fp, #-24]
	ldr r4, [fp, #-20]
	ldr r5, [fp, #-24]
	sdiv r6, r4, r5
	mul r7, r6, r5
	sub r5, r4, r7
	sub r5, r4, r7
	str r5, [fp, #-4]
	b .L64
.L66:
	ldr r4, [fp, #-16]
	ldr r5, [fp, #-12]
	mul r6, r4, r5
	ldr r4, [fp, #-24]
	sdiv r5, r6, r4
	mov r0, r5
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L68:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L69:
	mov r0, #0
	add sp, sp, #24
	pop {lr}
	pop {fp}
	bx lr
.L70:
	b .L66
	.text
	.global main
	.type main , %function
main:
push {fp}
push {lr}
mov fp, sp
sub sp, sp, #8
.L74:
	bl getint
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-8]
	bl getint
	add sp, sp, #0
	mov r4, r0
	str r4, [fp, #-4]
	ldr r4, [fp, #-4]
	ldr r5, [fp, #-8]
	mov r0, r5
	mov r1, r4
	bl gcd
	add sp, sp, #0
	mov r4, r0
	mov r0, r4
	add sp, sp, #8
	pop {lr}
	pop {fp}
	bx lr
addr_n:
	.word n
